﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Json;

namespace ConsoleWords
{
    public class Game
    {

        private string _pathToFile;

        public List<Player> _listPlayer;

        public string PathToFile
        {
            set { _pathToFile = value; }
        }

        public Game(string pathFile)
        {
            _pathToFile = pathFile;
            _listPlayer = new List<Player>();
            ReadFromFile();
        }

        /// <summary>
        /// Update score for all game
        /// </summary>
        public void AddScoreAllGame()
        {
            foreach(Player player in _listPlayer)
            {
                player.ScoreAllGame += player.Score;
            }
        }

        /// <summary>
        /// Update sscore for players
        /// </summary>
        public void UpdateScore(Player player)
        {
            Player findPlayer = _listPlayer.Find(p => p.ID == player.ID);
            findPlayer.Score = player.Score;
        }

        /// <summary>
        /// Show score for all games
        /// </summary>
        public void WriteTotalScore()
        {
            foreach(Player player in _listPlayer)
            {
                Console.WriteLine(Resource.Resources.String_score,player.Name,player.ScoreAllGame);
            }
        }

        /// <summary>
        /// Save game information
        /// </summary>
        public void SaveToFile()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<Player>));
            using (FileStream fs = new FileStream(_pathToFile, FileMode.OpenOrCreate))
            {
                serializer.WriteObject(fs, _listPlayer);
            }
        }

        /// <summary>
        /// Read game information
        /// </summary>
        public void ReadFromFile()
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(List<Player>));
            using (FileStream fs = new FileStream(_pathToFile, FileMode.OpenOrCreate))
            {
                try
                {
                    _listPlayer = (List<Player>)serializer.ReadObject(fs);
                }
                catch
                {
                    _listPlayer = new List<Player>();
                }
            }
        }

    }
}
