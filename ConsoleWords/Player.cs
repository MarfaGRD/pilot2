﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;

namespace ConsoleWords
{
    [DataContract]
    public class Player
    {
        [DataMember]
        private int _id;
        [DataMember]
        private int _scoreAllGame;

        private int _score;

        [DataMember]
        private string _name;

        public Player(int id_player, string name_player, int scoreAllGame_player)
        {
            _id = id_player;
            _name = name_player;
            _scoreAllGame = scoreAllGame_player;
            _score = 0;
        }

        public int ID
        {
            get
            {
                return _id;
            }
            set
            {
                _id = value;
            }
        }

        public string Name
        {
            get
            {
                return _name;
            }
            set
            {
                _name = value;
            }
        }

        public int ScoreAllGame
        {
            get
            {
                return _scoreAllGame;
            }
            set
            {
                _scoreAllGame = value;
            }
        }

        public int Score
        {
            get
            {
                return _score;
            }
            set
            {
                _score = value;
            }
        }
    }
}
