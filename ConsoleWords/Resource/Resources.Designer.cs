﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ConsoleWords.Resource {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("ConsoleWords.Resource.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Данное слово было введено ранее, повторите ввод.
        /// </summary>
        internal static string String_dictionary {
            get {
                return ResourceManager.GetString("String_dictionary", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на ничья.
        /// </summary>
        internal static string String_draw {
            get {
                return ResourceManager.GetString("String_draw", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Вы не успели ввести слово, игра окончена.
        /// </summary>
        internal static string String_end {
            get {
                return ResourceManager.GetString("String_end", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Номер игрока выбран неправильно.
        /// </summary>
        internal static string string_errorchoicePlayer {
            get {
                return ResourceManager.GetString("string_errorchoicePlayer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Введите первоначальное слово.
        /// </summary>
        internal static string String_first {
            get {
                return ResourceManager.GetString("String_first", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Первоначальное слово.
        /// </summary>
        internal static string String_initialWord {
            get {
                return ResourceManager.GetString("String_initialWord", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на  введите слово в течении минуты.
        /// </summary>
        internal static string String_input {
            get {
                return ResourceManager.GetString("String_input", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Слово введено неверно, нельзя вводить цифры, символы или слово на английском языке.
        /// </summary>
        internal static string String_inputOnlyLetters {
            get {
                return ResourceManager.GetString("String_inputOnlyLetters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Введите 0 для создания игрока.
        /// </summary>
        internal static string String_inputselectPlayer {
            get {
                return ResourceManager.GetString("String_inputselectPlayer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Количество букв в слове меньше 8 либо больше 30 символов.
        /// </summary>
        internal static string String_lenghtFirstWord {
            get {
                return ResourceManager.GetString("String_lenghtFirstWord", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Введите имя для игрока №.
        /// </summary>
        internal static string String_name {
            get {
                return ResourceManager.GetString("String_name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Игрок.
        /// </summary>
        internal static string String_player {
            get {
                return ResourceManager.GetString("String_player", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на [\a-zA-Z\d\W].
        /// </summary>
        internal static string String_regex {
            get {
                return ResourceManager.GetString("String_regex", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Введенное слово не соответствует буквам, повторите ввод.
        /// </summary>
        internal static string String_repeat {
            get {
                return ResourceManager.GetString("String_repeat", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Игрок: {0} очки: {1}.
        /// </summary>
        internal static string String_score {
            get {
                return ResourceManager.GetString("String_score", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Выберите игрока.
        /// </summary>
        internal static string String_selectPlayer {
            get {
                return ResourceManager.GetString("String_selectPlayer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на  выиграл.
        /// </summary>
        internal static string String_won {
            get {
                return ResourceManager.GetString("String_won", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Слова.
        /// </summary>
        internal static string String_word {
            get {
                return ResourceManager.GetString("String_word", resourceCulture);
            }
        }
    }
}
