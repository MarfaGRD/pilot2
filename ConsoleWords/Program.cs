﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Timers;

namespace ConsoleWords
{
    class Program
    {
        private static string _word;
        private static string _firstWord = string.Empty;
        private static string _name;

        private static List<string> _dictionary;
        private static List<Player> _players;

        private const int _countPlayer = 2;
        private static int _numberPlayer;


        public static int isRead;
        public static bool isExitPlay;

        public static Timer timer;

        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            Console.WriteLine("Введите 1 для перевода языка на английский(Enter 1 for English)");
            try
            {
                var k = Console.ReadLine();
                if (int.Parse(k) == 1)
                {
                    System.Threading.Thread.CurrentThread.CurrentUICulture = new CultureInfo("en");
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }

            Console.Clear();

            Game game = new Game("game.json");

            _players = new List<Player>();

            for (int i = 0; i < _countPlayer; i++)
            {
                Console.WriteLine(Resource.Resources.String_selectPlayer);

                foreach (Player pl in game._listPlayer)
                {
                    Console.WriteLine("{0}. {1}", pl.ID, pl.Name);
                }

                Console.WriteLine(Resource.Resources.String_inputselectPlayer);

                try
                {
                    _numberPlayer = int.Parse(Console.ReadLine());
                    if (i == 1)
                        if (_numberPlayer == _players[0].ID)
                            _numberPlayer = 0;
                }
                catch (Exception ex)
                {
                    _numberPlayer = 0;
                }

                if (_numberPlayer > game._listPlayer.Count || _numberPlayer <= 0)
                {
                    Console.WriteLine(Resource.Resources.String_name + (i + 1));
                    _name = Console.ReadLine();
                    if (string.IsNullOrEmpty(_name))
                    {
                        _name = Resource.Resources.String_player + (i + 1);
                    }

                    Player newPlayer = new Player(game._listPlayer.Count + 1, _name, 0);

                    game._listPlayer.Add(newPlayer);

                    _players.Add(newPlayer);
                }
                else
                {
                    _players.Add(game._listPlayer.Find(p => p.ID == _numberPlayer));
                }
            }

            #region input and check initial word
            bool isInitialWord = true;
            do
            {
                Console.WriteLine(Resource.Resources.String_first);
                _firstWord = Console.ReadLine();
                if (_firstWord.Length >= 8 && _firstWord.Length <= 30)
                {
                    isInitialWord = false;
                    Regex r = new Regex(@Resource.Resources.String_regex);
                    isInitialWord = r.IsMatch(_firstWord);
                    if (isInitialWord)
                    {
                        Console.WriteLine(Resource.Resources.String_inputOnlyLetters);
                    }
                }
                else
                {
                    Console.WriteLine(Resource.Resources.String_lenghtFirstWord);
                }
            } while (isInitialWord);
            Console.Clear();
            #endregion

            _dictionary = new List<string>();
            isExitPlay = false;
            int number_player = 0;
            while (!isExitPlay)
            {
                Console.WriteLine($"{Resource.Resources.String_initialWord} : {_firstWord}");


                if (number_player == _players.Count)
                {
                    number_player = 0;
                }

                try
                {
                    Console.WriteLine($"{ _players[number_player].Name },{ Resource.Resources.String_input}");

                    _word = string.Empty;

                    timer = new Timer(1000);
                    isRead = 60;
                    timer.Elapsed += CheckReadWord;
                    timer.Start();

                    _word = Console.ReadLine();
                    if (isExitPlay)
                    {
                        throw new TimeoutException();
                    }

                    switch (_word)
                    {
                        case "/show-words":   //schow all words in this game
                            Console.Write("{0}:", Resource.Resources.String_word);
                            foreach (string s in _dictionary)
                                Console.Write(" {0}", s);
                            Console.Write("\n");
                            break;
                        case "/score":
                            foreach (Player player in _players)
                            {
                                Console.WriteLine(Resource.Resources.String_score, player.Name, player.ScoreAllGame);
                            }
                            break;
                        case "/total-score":
                            game.WriteTotalScore();
                            break;
                        default:
                            //Word check
                            if (CheckWord(_firstWord, _word) || string.IsNullOrEmpty(_word))
                            {
                                Console.Clear();
                                Console.WriteLine(Resource.Resources.String_repeat);
                            }
                            else
                            {
                                timer.Stop();

                                //Dictionary check
                                bool isDictionary = false;
                                foreach (string s in _dictionary)
                                    if (s.Equals(_word))
                                    {
                                        isDictionary = true;
                                    }

                                Console.Clear();

                                if (isDictionary && !_dictionary.Equals(null))
                                {
                                    Console.WriteLine(Resource.Resources.String_dictionary);
                                }
                                else
                                {
                                    _dictionary.Add(_word);
                                    _players[number_player].Score++;
                                    number_player++;
                                }
                            }
                            break;
                    }
                }
                catch (TimeoutException)
                {
                    isExitPlay = true;
                    Console.WriteLine(Resource.Resources.String_end);
                    number_player = (number_player == 0) ? 1 : 0;
                }
            }
            if (_dictionary.Count > 0)
            {
                Console.WriteLine(_players[number_player].Name + Resource.Resources.String_won);
            }
            else
            {
                Console.WriteLine(Resource.Resources.String_draw);
            }

            foreach (Player player in _players)
            {
                game.UpdateScore(player);
            }


            game.AddScoreAllGame();

            game.SaveToFile();

            Console.ReadKey();
        }

        /// <summary>
        /// Matching letters in words
        /// </summary>
        /// <param name="firstword">Initial word</param>
        /// <param name="word">Typed word</param>
        private static bool CheckWord(string firstWord, string word)
        {
            bool f = true;

            var check1 = firstWord.ToLower()
                .Select(c => new { val = c, count = firstWord.ToLower().Count(c2 => c2 == c) })
                .Distinct();
            var check2 = word.ToLower()
                .Select(c => new { val = c, count = word.ToLower().Count(c2 => c2 == c) })
                .Distinct();
            if (check2.All(c => c.count <= check1.FirstOrDefault(c2 => c2.val == c.val)?.count))
                f = false;
            return f;
        }

        /// <summary>
        /// Countdown for reading entered word
        /// </summary>
        private static void CheckReadWord(object sender, ElapsedEventArgs e)
        {
            isRead--;
            if (isRead == 0)
            {
                timer.Close();
                timer.Dispose();
                isExitPlay = true;
            }
        }
    }
}
